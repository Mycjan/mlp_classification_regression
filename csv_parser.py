import csv
from typing import List


def parse_regression(file_name: str, data_count: int, headers_row_count: int) -> (List[List[float]], List[List[float]]):
    with open(file_name) as f:
        reader = csv.reader(f)
        loaded_data = list(reader)

    data = list()
    results = list()
    for i in range(headers_row_count, len(loaded_data)):
        data.append(loaded_data[i][:data_count])
        data[i - 1] = [float(i) for i in data[i - 1]]
        results.append(loaded_data[i][data_count:])
        results[i - 1] = [float(i) for i in results[i - 1]]

    return data, results


def parse_classification(file_name: str, data_count: int, headers_row_count: int) -> (List[List[float]], List[List[float]]):
    with open(file_name) as f:
        reader = csv.reader(f)
        loaded_data = list(reader)

    data = list()
    results = list()
    for i in range(headers_row_count, len(loaded_data)):
        data.append(loaded_data[i][:data_count])
        data[i - 1] = [float(i) for i in data[i - 1]]
        results.append(loaded_data[i][data_count:])
        results[i - 1] = [float(i) for i in results[i - 1]]

    max_elem = int(max(results)[0])
    min_elem = int(min(results)[0])
    results_vec = list()
    for i in range(len(results)):
        results_vec.append([0] * (max_elem - min_elem + 1))
        results_vec[i][int(results[i][0]) - min_elem] = 1

    return data, results_vec
