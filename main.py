from csv_parser import parse_classification
from csv_parser import parse_regression
from NetworkModel import NetworkModel
import MNIST_parser
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def sigmoid_d(x):
    return sigmoid(x) * (1 - sigmoid(x))


def m_sigmoid(x):
    return -sigmoid(x)


def m_sigmoid_d(x):
    return -sigmoid_d(x)


def reLU(x):
    return max(0, x)


def reLU_d(x):
    if x > 0:
        return 1
    return 0


def tanh(x):
    return np.tanh(x)


def tanh_d(x):
    return 1-np.tanh(x)*np.tanh(x)


def linear_d(x):
    return 1


def linear(x):
    return x


def second_squared_error(expected, actual):
    er = 0
    for i in range(len(expected)):
        er = er + ((expected[i] - actual[i]) ** 2) * (expected[i] ** 2)
    return er / 2


def second_squared_error_d(expected, actual, actual_d):
    return (expected - actual) * actual_d * (expected ** 2)


def mean_squared_error(expected, actual):
    er = 0
    for i in range(len(expected)):
        er = er + (expected[i] - actual[i]) ** 2
    return er / 2


def mean_squared_error_d(expected, actual, actual_d):
    return (expected - actual) * actual_d


def absolute_error(expected, actual):
    er = 0
    for i in range(len(expected)):
        er = er + abs(expected[i] - actual[i])
    return er


def absolute_error_d(expected, actual, actual_d):
    if actual - expected > 0:
        return -actual_d

    return actual_d


def get_class_number(v):
    return v.index(max(v))


def train_test_regression(nm: NetworkModel, train_filename, test_filename):
    (train_data, train_results) = parse_regression(train_filename, 1, 1)

    x_v, y_v = nm.train_network(train_data, train_results, 2000)
    plt.plot(x_v, y_v)
    plt.xlabel('Iteration')
    plt.ylabel('Error')
    plt.title("Error in iteration")
    plt.savefig("error.png")

    (test_data, test_results) = parse_regression(test_filename, 1, 1)
    xs = []
    ys = []
    cxs = []
    cys = []
    for idx in range(len(test_data)):
        v = nm.evaluate(test_data[idx])
        xs.append(test_data[idx][0])
        ys.append(v[0])
        cxs.append(test_data[idx][0])
        cys.append(test_results[idx])

    plt.close()
    plt.scatter(xs, ys, color='blue', s=10)
    plt.title("Network regression")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig("Regression.png")

    plt.close()
    plt.scatter(cxs, cys, color='blue', s=10)
    plt.title("Correct regression")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig("Correct_regression.png")


def train_test_classification(nm: NetworkModel, train_filename, test_filename):
    (train_data, train_results) = parse_classification(train_filename, 2, 1)

    nm.read_dict_from_file('test_dict.json')
    x_v, y_v = nm.train_network(train_data, train_results, 200)
    nm.save_dict_to_file('test_dict.json')
    plt.plot(x_v, y_v)
    plt.xlabel('Iteration')
    plt.ylabel('Error')
    plt.title("Error in iteration")
    plt.savefig("error.png")

    (test_data, test_results) = parse_classification(test_filename, 2, 1)
    classes_count = len(test_results[0])
    xs = [[] for _ in range(classes_count)]
    ys = [[] for _ in range(classes_count)]
    cxs = [[] for _ in range(classes_count)]
    cys = [[] for _ in range(classes_count)]
    confusion_matrix = [[0 for _ in range(classes_count)] for _ in range(classes_count)]
    for idx in range(len(test_data)):
        v = nm.evaluate(test_data[idx])
        class_name = get_class_number(v)
        xs[class_name].append(test_data[idx][0])
        ys[class_name].append(test_data[idx][1])
        cclass_name = get_class_number(test_results[idx])
        cxs[cclass_name].append(test_data[idx][0])
        cys[cclass_name].append(test_data[idx][1])
        confusion_matrix[class_name][cclass_name] = confusion_matrix[class_name][cclass_name] + 1

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    color_fun = cm.get_cmap('rainbow', classes_count)
    for idx in range(classes_count):
        ax.scatter(xs[idx], ys[idx], color=color_fun(idx), s=10)

    plt.title("Network classification")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig("Classification.png")

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    for idx in range(classes_count):
        ax.scatter(cxs[idx], cys[idx], color=color_fun(idx), s=10)
    plt.title("Correct classification")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig("Correct_classification.png")

    plt.matshow(confusion_matrix, cmap=cm.get_cmap('Blues'))
    for i in range(classes_count):
        for j in range(classes_count):
            c = confusion_matrix[j][i]
            plt.text(i, j, str(c), va='center', ha='center')

    plt.xlabel("Actual class")
    plt.ylabel("Predicted class")
    plt.title("Confusion matrix")
    plt.savefig("Confusion_matrix.png")


def test_mnist(nm):
    images_data = MNIST_parser.parse_images("TrainTestData\\t10k-images.idx3-ubyte")
    labels_data = MNIST_parser.parse_labels("TrainTestData\\t10k-labels.idx1-ubyte")

    images_vectors = MNIST_parser.convert_image_to_vector(images_data, range(len(images_data)))
    images_labels = MNIST_parser.convert_label_to_vector(labels_data, range(len(images_data)))

    correct = 0
    for i in range(len(images_vectors)):
        output = nm.evaluate(images_vectors[i])
        output_class = get_class_number(output)
        correct_class = get_class_number(images_labels[i])

        if output_class == correct_class:
            correct = correct + 1

        if i % 100 == 0:
            print(i)
        if i != 0 and i % 1000 == 0:
            print(correct / i)

    return correct / len(images_vectors)


if __name__ == '__main__':
    # nm = NetworkModel(2, [2, 2], 1234, 2, 0.1, tanh, sigmoid, tanh_d, sigmoid_d, mean_squared_error, mean_squared_error_d)
    # train_test_classification(nm, "SN_projekt1_test\\classification\\data.noisyXOR.train.100.csv",
    #                          "SN_projekt1_test\\classification\\data.noisyXOR.test.100.csv")


    # nmr = NetworkModel(3, [1, 20, 1], 1234, 0.05, 0.1, tanh, linear, tanh_d, linear_d, mean_squared_error, mean_squared_error_d)
    # train_test_regression(nmr, "SN_projekt1_test\\Regression\\data.square.train.100.csv",
    #                         "SN_projekt1_test\\Regression\\data.square.test.100.csv")

    images_data = MNIST_parser.parse_images("TrainTestData\\train-images.idx3-ubyte")
    labels_data = MNIST_parser.parse_labels("TrainTestData\\train-labels.idx1-ubyte")
    data100 = MNIST_parser.get_first_elems_count(labels_data, 100)
    images_vectors = MNIST_parser.convert_image_to_vector(images_data, range(len(images_data)))
    images_labels = MNIST_parser.convert_label_to_vector(labels_data, range(len(images_data)))
    nm = NetworkModel(3, [784, 30, 10], 1236, 1, 0.1, tanh, sigmoid, tanh_d, sigmoid_d, mean_squared_error, mean_squared_error_d)
    nm.read_dict_from_file('network_784_30_10_b10_fd_e10.json')
    nm.train_network(images_vectors, images_labels, 10, 1, 'network_784_30_10_b10_fd.json')

    acc = test_mnist(nm)
    print(acc)

