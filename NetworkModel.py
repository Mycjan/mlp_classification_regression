from typing import List
import numpy as np
from ast import literal_eval
import json
import random


class NetworkModel:
    epsilon: float
    ni: float
    n: int  # liczba wszystkich warstw 
    sizes: List[int]  # wielkości wszystkich warst
    actual_weights: dict = {}  # [l, i, j] aktualne wagi na krawędziach od wezla i w warstwie l do wezla j w warstwie l+1
    actual_values = []  # aktualne wartości argumentow w perceptronach [l, i] - wartosc i tego neuronu w l warstwie (zawiera bias)
    actual_outputs = [] #wyjscia wszystkich neuronow
    activation_fun_hidden = None
    activation_fun_hidden_d = None
    activation_fun_last = None
    activation_fun_last_d = None
    error_fun = None
    error_fun_d = None

    def __init__(self, n: int, sizes: List[int], seed: int, ni: float, epsilon: float, activation_fun_hidden,
                 activation_fun_last, activation_fun_hidden_d, activation_fun_last_d, error_fun, error_fun_d):
        self.epsilon = epsilon
        self.n = n
        self.sizes = sizes
        self.ni = ni
        self.activation_fun_hidden = activation_fun_hidden
        self.activation_fun_last = activation_fun_last
        self.activation_fun_hidden_d = activation_fun_hidden_d
        self.activation_fun_last_d = activation_fun_last_d
        self.error_fun = error_fun
        self.error_fun_d = error_fun_d
        np.random.seed(seed)
        bias = 1
        for i in range(self.n - 1):
            if i == self.n - 2:
                bias = 0
            for p in range(self.sizes[i] + 1):  # +1 cause one extra neuron is needed as bias
                for q in range(bias, self.sizes[i + 1] + bias):
                    self.actual_weights[(i, p, q)] = np.random.random_sample() * 2 - 1  # ewentualnie do zmiany

        self.actual_values = [[]] * self.n

        for i in range(self.n - 1):
            self.actual_values[i] = [1] + ([0] * sizes[i])  # extra neuron value for bias

        self.actual_values[self.n - 1] = [0] * sizes[self.n - 1]

    def evaluate(self, x):
        v = self.forward_propagation(x)
        for i in range(len(v)):
            v[i] = self.activation_function(self.n - 1)(v[i])

        return v

    def activation_function(self, idx: int):
        if idx == self.n - 1: #last layer
            return self.activation_fun_last
        else:
            return self.activation_fun_hidden

    #d like derivative
    def activation_function_d(self, idx: int):
        if idx == self.n - 1: #last layer
            return self.activation_fun_last_d
        else:
            return self.activation_fun_hidden_d

    def forward_propagation(self, x):
        for i in range(self.n - 1):
            for j in range(1, self.sizes[i] + 1):
                self.actual_values[i][j] = 0

        for i in range(self.sizes[self.n - 1]):
            self.actual_values[self.n - 1][i] = 0

        for i in range(len(x)):
            self.actual_values[0][i + 1] = x[i]  # sprawdzić,  +1 cause 0 cell is bias

        bias = 1
        for layer in range(1, self.n):
            if layer == self.n - 1:
                bias = 0

            for q in range(bias, self.sizes[layer] + bias):  # start with 1 cause bias value is not evaluated
                for p in range(self.sizes[layer - 1] + 1):  # +1 cause bias value is used to evaluate neuron value
                    self.actual_values[layer][q] += self.activation_function(layer - 1)(self.actual_values[layer - 1][p]) * self.actual_weights[layer - 1, p, q]

        return self.actual_values[self.n - 1]

    def back_propagation(self, expectedY: list):
        #calculate b
        b = [[]] * self.n
        for layer in reversed(range(self.n)):
            if layer == self.n - 1:
                b[layer] = [0] * self.sizes[layer]
                #last layer, vector b is directly from global error
                for i in range(self.sizes[layer]):
                    b[layer][i] = self.error_fun_d(expectedY[i], self.activation_function(layer)(self.actual_values[layer][i]), self.activation_function_d(layer)(self.actual_values[layer][i]))
            else:
                if layer == self.n - 2:
                    bias = 0
                else:
                    bias = 1
                b[layer] = [0] * (self.sizes[layer] + 1)
                for i in range(self.sizes[layer] + 1): # + 1 bias node
                    error = 0 # its saying how strong does this node affect on nodes in next layer
                    for j in range(bias, self.sizes[layer + 1] + bias):
                        error += b[layer + 1][j] * self.actual_weights[layer, i, j]
                    b[layer][i] = error * self.activation_function_d(layer)(self.actual_values[layer][i]) 

        #correct weights
        weights_update_dict = {}
        for layer in range(self.n - 1):
            bias = 1
            if layer == self.n - 2:
                bias = 0
            for p in range(self.sizes[layer] + 1):
                for q in range(bias, self.sizes[layer + 1] + bias):
                    #input is a 0th layer
                    weights_update_dict[(layer, p, q)] = b[layer + 1][q] * self.activation_function(layer)(self.actual_values[layer][p])
        return weights_update_dict

    def update_weights(self, update_dict):
        for key in self.actual_weights:
            self.actual_weights[key] = self.actual_weights[key] + self.ni * update_dict[key]

    @staticmethod
    def update_dict(local_dict: dict, weights_dict, input_count):
        for key in local_dict:
            if key in weights_dict:
                weights_dict[key] += local_dict[key] / input_count
            else:
                weights_dict[key] = local_dict[key] / input_count

    def train_network(self, train_data, train_expected_results, batch_size, iterations_count, filename):  # do sprawdzenia
        x_v = list()
        y_v = list()
        c = list(zip(train_data, train_expected_results))

        for it in range(iterations_count):
            print("Epoch: " + str(it))
            random.shuffle(c)
            shuffled_data, shuffled_results = zip(*c)
            data_batches = [shuffled_data[k:k + batch_size] for k in range(0, len(train_data), batch_size)]
            results_batches = [shuffled_results[k:k + batch_size] for k in range(0, len(train_data), batch_size)]
            idx = 0

            for (batch, result) in zip(data_batches, results_batches):
                weights_dict = {}
                error = list()
                for i in range(len(batch)):
                    output = self.forward_propagation(batch[i])
                    actual_output = [self.activation_fun_last(v) for v in output]
                    error.append(self.error_fun(result[i], actual_output))
                    local_dict = self.back_propagation(result[i])
                    self.update_dict(local_dict, weights_dict, batch_size)


                #back propagation
                self.update_weights(weights_dict)

                mean_error = np.mean(error)
                x_v.append(idx)
                y_v.append(mean_error)
                idx = idx + 1
                print("Batch number = " + str(idx) + "\nNorm = " + str(mean_error))

            self.save_dict_to_file(filename)

        return x_v, y_v

    def save_dict_to_file(self, filename):
        with open(filename, 'w') as f:
            json.dump({str(k): v for k, v in self.actual_weights.items()}, f)

    def read_dict_from_file(self, filename):
        with open(filename, 'r') as f:
            obj = json.load(f)
            self.actual_weights = { literal_eval(k): v for k, v in obj.items() }







